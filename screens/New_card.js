import * as React from "react";
import {
  Button,
  Image,
  View,
  Text,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView
} from "react-native";
import ImgToBase64 from 'react-native-image-base64';
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import { TextField } from "react-native-material-textfield";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment";
import { AlertHelper } from "../src/AlertHelper";

export default class New_card extends React.Component {
  state = {
    isDateTimePickerVisible: false,
    isDateTimePickerVisible2: false,
    image: "http://placehold.it/480x270",
    name: "",
    subtitle: "",
    resume: ""
  };

  handleSubmit = () => {
    console.log(this.state.name);

    if (this.state.name != "") {
      fetch(global.URL + "Service/WS/Events?token=" + this.state.uid, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          name: this.state.name,
          date: this.state.selectedDate,
          image_scr: "http://placehold.it/480x270",
          subtitle: this.state.subtitle,
          resume: this.state.resume,
          map: "map",
          image: this.state.image.base64,
          price: 5
        })
      })
        .then(response => {
          AlertHelper.setOnClose(() => {
            this.props.navigation.goBack();
            AlertHelper.setOnClose(() => undefined);
          });
          AlertHelper.show(
            "success",
            "Success",
            "Evento registrado com Sucesso."
          );
        })
        .catch(error => {
          AlertHelper.setOnClose(() => undefined);
          AlertHelper.show(
            "error",
            "Erro",
            "Erro inesperado ao salvar o Evento"
          );
        });
    } else {
      AlertHelper.show(
        "error",
        "Campo Obrigatorio",
        "Campo Nome precisa ser informado."
      );
    }
  };

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
    const newDate = moment(new Date(date.toString())).format(
      "DD-MM-YYYY HH:mm"
    );
    this.setState({ selectedDate: newDate });
    this.hideDateTimePicker();
  };

  render() {
    let { image } = this.state;
    let imageUri = image ? `data:image/jpg;base64,${image.base64}` : null;
    return (
      <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <ScrollView>
            <View style={{ padding: 8 }}>
              <Image
                source={{ uri: imageUri }}
                style={{ width: "100%", aspectRatio: 16 / 9 }}
              />
              <Button title="Selecionar Imagem" onPress={this._pickImage} />
              <Text style={styles.text}>{this.state.selectedDate}</Text>
              <Button
                title="Show DatePicker"
                onPress={this.showDateTimePicker}
              />
              <DateTimePicker
                locale="pt_BR"
                mode="datetime"
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this.handleDatePicked}
                onCancel={this.hideDateTimePicker}
              />
              <TextField
                label="Nome do evento"
                onChangeText={name => this.setState({ name })}
                value={this.state.name}
              />

              <TextField
                label="SubTitulo"
                onChangeText={subtitle => this.setState({ subtitle })}
                value={this.state.subtitle}
                multiline={true}
                numberOfLines={2}
              />
              <TextField
                label="Resumo"
                onChangeText={resume => this.setState({ resume })}
                value={this.state.resume}
                multiline={true}
                numberOfLines={5}
              />

              <Button title="Enviar!" onPress={this.handleSubmit} />
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }

  componentDidMount() {
    this.getPermissionAsync();
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      base64: true,
      allowsEditing: true,
      aspect: [16, 9]
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result});
    }
  };
}

const styles = StyleSheet.create({
  inputContainer: {
    paddingTop: 15
  },
  text: {
    marginVertical: 10
  },
  textInput: {
    margin: 5,
    borderRadius: 2,
    height: 40,
    borderColor: "#7a42f4",
    borderWidth: 0.5
  }
});
