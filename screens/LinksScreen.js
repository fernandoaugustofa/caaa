import React from 'react';
import {
  ScrollView,
  StyleSheet, View, Text, FlatList
} from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { SearchBar, List, ListItem } from 'react-native-elements';
import {
  Card,
  CardTitle,
  CardContent,
  CardAction,
  CardButton,
  CardImage
} from 'react-native-material-cards'

const list = [
  {
    key: '0',
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    key: '1',
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    key: '2',
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    key: '3',
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    key: '4',
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    key: '5',
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    key: '6',
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    key: '7',
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    key: '8',
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
]

export default function LinksScreen() {
  return (
    <View style={styles.container}>
        <FlatList
          data={list}
          renderItem={({item}) => 
            <ListItem
              roundAvatar
              title={item.name}
              subtitle={item.subtitle}
              avatar={{uri:item.avatar_url}}
            />
          }
        />
      </View>
  );
}

LinksScreen.navigationOptions = {
  title: 'Loja',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
  root: {
    flex: 1,
    backgroundColor: "#FFF",
    flexWrap: "nowrap",
    elevation: 3,
    borderRadius: 0,
    borderColor: "#CCC",
    borderWidth: 6,
    shadowOffset: {
      height: 2,
      width: -2
    },
    shadowColor: "#000",
    shadowOpacity: 0.1,
    shadowRadius: 1.5,
    overflow: "hidden"
  },
  rect: {
    top: 0,
    left: 0,
    width: 97,
    height: 80,
    backgroundColor: "rgba(230, 230, 230,1)",
    position: "absolute"
  },
  text: {
    top: 8,
    left: 109,
    width: 250,
    height: 32,
    color: "#121212",
    position: "absolute",
    fontSize: 18
  },
  text2: {
    top: 40,
    left: 109,
    color: "#121212",
    position: "absolute",
    fontSize: 14
  }
});
