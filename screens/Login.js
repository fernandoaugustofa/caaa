import React from "react";
import {
  StyleSheet,
  View,
  TextInput,
  KeyboardAvoidingView,
  Image,
  TouchableOpacity,
  Text
} from "react-native";
import { AlertHelper } from "../src/AlertHelper";
import { db } from "../src/Firebase/Config";

export default class SignInScreen extends React.Component {
  static navigationOptions = {
    header: null
  };
  componentDidMount() {
    db.auth().onAuthStateChanged(user => {
      if (user) {
        AlertHelper.show("success", "Bem Vindo !!", "");
        this.props.navigation.navigate("Main");
      }
    });
  }
  componentWillUnmount() {
    AlertHelper.setOnClose(() => undefined);
  }
  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <View style={styles.loginContainer}>
          <Image
            resizeMode="contain"
            style={styles.logo}
            source={{ uri: "http://placehold.it/480x270" }}
          />
        </View>
        <View style={styles2.formContainer}>
          <View style={styles2.container}>
            <TextInput
              style={styles2.input}
              autoCapitalize="none"
              onSubmitEditing={() => this.passwordInput.focus()}
              autoCorrect={false}
              keyboardType="email-address"
              returnKeyType="next"
              placeholder="Email ou Username"
              placeholderTextColor="rgba(225,225,225,0.7)"
              onChangeText={text => this.setState({ email: text })}
            />
            <TextInput
              style={styles2.input}
              returnKeyType="go"
              ref={input => (this.passwordInput = input)}
              placeholder="Password"
              placeholderTextColor="rgba(225,225,225,0.7)"
              secureTextEntry
              onChangeText={text => this.setState({ password: text })}
            />
            <TouchableOpacity
              style={styles2.buttonContainer2}
              onPress={() => this.props.navigation.navigate("SignUp")}
            >
              <Text style={styles2.buttonText}>Cadastrar</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles2.buttonContainer}
              onPress={this.handleLogin}
            >
              <Text style={styles2.buttonText}>LOGIN</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }

  handleLogin = () => {
    const { email, password } = this.state;
    db.auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate("Main"))
      .catch(error => AlertHelper.show("error", "Erro", error));
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000"
  },
  loginContainer: {
    alignItems: "center",
    flexGrow: 1,
    justifyContent: "center"
  },
  logo: {
    position: "absolute",
    width: 300,
    height: 100
  },
  title: {
    color: "#FFF",
    marginTop: 120,
    width: 180,
    textAlign: "center",
    opacity: 0.9
  }
});

const styles2 = StyleSheet.create({
  container: {
    padding: 20
  },
  input: {
    height: 40,
    backgroundColor: "rgba(225,225,225,0.2)",
    marginBottom: 10,
    padding: 10,
    color: "#fff"
  },
  buttonContainer: {
    backgroundColor: "#2c3e50",
    paddingVertical: 15
  },
  buttonContainer2: {
    paddingTop: 5,
    paddingBottom: 35
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontWeight: "700"
  },
  loginButton: {
    backgroundColor: "#2980b6",
    color: "#fff"
  }
});
