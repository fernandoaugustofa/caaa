import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { db } from "../src/Firebase/Config";
import QRCode from 'react-native-qrcode-svg';

export default class Profile extends Component {
  constructor(props) {
    super(props);  
    this.state = {
      isLoading: true
    }  
  }
  componentDidMount() {
    User = db.auth().currentUser;
    this.state = {
      isLoading: true,
      uid: User.uid
    };
    this.GetJson();
  }
  GetJson = () => {
    fetch(global.URL +'Service/WS/user?token=' + this.state.uid)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson
          },
          function () { }
        );
        console.log(this.state)
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.header}></View>
        <Image style={styles.avatar} source={{ uri: this.state.dataSource.image_scr }} />
        <View style={styles.body}>
          <View style={styles.bodyContent}>
            <Text style={styles.name}>{this.state.dataSource.name}</Text>
            <Text style={styles.info}>{this.state.dataSource.nick}</Text>
            <Text style={styles.description}>{this.state.dataSource.squad}</Text>
            <Text style={styles.description}>{this.state.dataSource.level}</Text>
            
            
          </View>
          <View style={styles.bodyContent}>
            <QRCode value={this.state.uid} color='#56555e' />
          </View>          
        </View>
      </View>
    );
  }
}
Profile.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#56555e',
    height: 100,
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: 30
  },
  name: {
    fontSize: 22,
    color: "#56555e",
    fontWeight: '600',
    textAlign: 'center'
  },
  body: {
    marginTop: 40,
  },
  bodyContent: {
    alignItems: 'center',
    padding: 30,
  },
  name: {
    fontSize: 28,
    color: "#696969",
    fontWeight: "600"
  },
  info: {
    fontSize: 16,
    color: "#00BFFF",
    marginTop: 10
  },
  description: {
    fontSize: 16,
    color: "#696969",
    marginTop: 10,
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
    backgroundColor: "#00BFFF",
  },
});