import React from 'react'
import {
    StyleSheet,
    View,
    TextInput,
    KeyboardAvoidingView,
    Image,
    TouchableOpacity,
    Text
} from "react-native";
import { db } from "../src/Firebase/Config";
import DropdownAlert from "react-native-dropdownalert";

export default class SignUpScreen extends React.Component {
    state = { email: '', password: '', repassword: '', errorMessage: null }
    static navigationOptions = {
        header: null
    };

    handleSignUp = () => {
        const { email, password, repassword } = this.state
        if (password == repassword) {
            db.auth()
                .createUserWithEmailAndPassword(email, password)
                .then(user => this.props.navigation.navigate('Main'))
                .catch(error => this.setState({ errorMessage: error.message }))
        } else {
            this.setState({ errorMessage: error.message })
        }
       
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={styles.loginContainer}>
                    <DropdownAlert ref={ref => (this.dropDownAlertRef = ref)} />
                    <Image
                        resizeMode="contain"
                        style={styles.logo}
                        source={require('../assets/images/icon.png')}
                    />
                </View>
                <View style={styles2.formContainer}>
                    <View style={styles2.container}>
                        <TextInput
                            style={styles2.input}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.passwordInput.focus()}
                            autoCorrect={false}
                            keyboardType="email-address"
                            returnKeyType="next"
                            placeholder="Email"
                            placeholderTextColor="rgba(225,225,225,0.7)"
                            onChangeText={text => this.setState({ email: text })}
                        />
                        <TextInput
                            style={styles2.input}
                            returnKeyType="go"
                            ref={input => (this.passwordInput = input)}
                            placeholder="Senha"
                            placeholderTextColor="rgba(225,225,225,0.7)"
                            secureTextEntry
                            onChangeText={text => this.setState({ password: text })}
                        />
                        <TextInput
                            style={styles2.input}
                            returnKeyType="go"
                            ref={input => (this.passwordInput = input)}
                            placeholder="Confirmar Senha"
                            placeholderTextColor="rgba(225,225,225,0.7)"
                            secureTextEntry
                            onChangeText={text => this.setState({ password: text })}
                        />
                        <TouchableOpacity
                            style={styles2.buttonContainer2}
                            onPress={() => this.props.navigation.navigate("Login")}
                        >
                            <Text style={styles2.buttonText}>Login</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles2.buttonContainer}
                            onPress={this.handleLogin}
                        >
                            <Text style={styles2.buttonText}>SingUp</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000"
    },
    loginContainer: {
        alignItems: "center",
        flexGrow: 1,
        justifyContent: "center"
    },
    logo: {
        position: "absolute",
        width: 300,
        height: 100
    },
    title: {
        color: "#FFF",
        marginTop: 120,
        width: 180,
        textAlign: "center",
        opacity: 0.9
    }
});

const styles2 = StyleSheet.create({
    container: {
        padding: 20
    },
    input: {
        height: 40,
        backgroundColor: "rgba(225,225,225,0.2)",
        marginBottom: 10,
        padding: 10,
        color: "#fff"
    },
    buttonContainer: {
        backgroundColor: "#2980b6",
        paddingVertical: 15
    },
    buttonContainer2: {
        paddingTop: 5,
        paddingBottom: 35

    },
    buttonText: {
        color: "#fff",
        textAlign: "center",
        fontWeight: "700"
    },
    loginButton: {
        backgroundColor: "#2980b6",
        color: "#fff"
    }
});
