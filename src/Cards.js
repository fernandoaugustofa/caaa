import React from "react";
import {
  FlatList,
  ActivityIndicator,
  Text,
  View,
  StyleSheet,
  Platform,
  TouchableOpacity,
  RefreshControl
} from "react-native";
import {
  Card,
  CardTitle,
  CardContent,
  CardAction,
  CardButton,
  CardImage
} from "react-native-material-cards";
import { db } from "../src/Firebase/Config";
import uuid from "uuid";


export default class Cards extends React.Component {
  constructor(props) {
    super(props);
    
    User = db.auth().currentUser
    this.state = { isLoading: true,
      name: User.displayName,
      email: User.email,
      photoUrl: User.photoURL,
      emailVerified: User.emailVerified,
      uid: User.uid
    };
  }

  componentDidMount() {
    this.GetJson();
  }

  join = (id) => {
    fetch(global.URL + 'Service/WS/join?event=' + id + '&operator=' + this.state.uid, {
      method: 'POST',
    }).then((response) => {
      this.GetJson();
    })
      .catch((error) => {
        console.error(error);
      });
  }

  leave = (id) => {
    fetch(global.URL + 'Service/WS/leave?event=' + id + '&operator=' + this.state.uid, {
      method: 'POST',
    }).then((response) =>{
      this.GetJson();
    })
      .catch((error) => {
        console.error(error);
      });
  }


  GetJson = () => {
    this.setState({ isRefreshing: true });
    fetch(global.URL +'Service/WS/Events?token=' + this.state.uid )
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState(
          {
            isRefreshing: false,
            isLoading: false,
            dataSource: responseJson
          },
          function () { }
        );
      })
      .catch((error) => {
        console.error(error);
      });

    
  }

  onRefresh() {

  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <View style={{ flex: 1, paddingTop: 20 }}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({ item }) => (
            <TouchableOpacity
              key={item.id}
              onPress={() => {
                this.props.props2.navigation.navigate("Links2", {
                  key: item.key,
                  id: item.id,
                  name: item.name,
                  image_scr: item.image_scr,
                  date: item.date,
                  subtitle: item.subtitle,
                  resume: item.resume,
                  bottom: this.bottom(item.ative, item.id)
                });
              }}
            >
              <Card style={styles.card}>
                <CardImage source={{ uri: global.URL +"Image?name="+item.image_scr }} title={item.date} />
                <CardTitle title={item.name} subtitle={item.subtitle} />
                <CardContent text={item.resume} />
                <CardAction separator={true} inColumn={false}>
                  {this.bottom(item.ative, item.id)}
                  <CardButton
                    onPress={() => {
                      console.log("teste");
                    }}
                    title="Ler Mais ..."
                    color="blue"
                  />
                </CardAction>
                <CardAction separator={true} />
              </Card>
            </TouchableOpacity>
          )}
          refreshControl={
            <RefreshControl
              //refresh control used for the Pull to Refresh
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh.bind(this)}
            />
          }
          keyExtractor={({ id }, index) => id}
        />
      </View>
    );
  }

  bottom(active, id){
    console.log(active);
    if (active) {
      return <CardButton
        style={styles.Center}
        onPress={() => {
          this.leave(id)
        }}
        title="Desistir!"
        color="rgba(255,0,0,0.7)"
      />;
    }else{
      return <CardButton
        onPress={() => { 
          this.join(id)
        }}
        title="Participar !"
        color="green"
      />;
    }
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  card: {
    shadowColor: "#000",
    shadowOpacity: 0.1,
    shadowRadius: 1.5
  },
  developmentModeText: {
    marginBottom: 20,
    color: "rgba(0,0,0,0.4)",
    fontSize: 14,
    lineHeight: 19,
    textAlign: "center"
  },
  contentContainer: {
    paddingTop: 30
  },
  welcomeContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: "contain",
    marginTop: 3,
    marginLeft: -10
  },
  getStartedContainer: {
    alignItems: "center",
    marginHorizontal: 50
  },
  homeScreenFilename: {
    marginVertical: 7
  },
  codeHighlightText: {
    color: "rgba(96,100,109, 0.8)"
  },
  codeHighlightContainer: {
    backgroundColor: "rgba(0,0,0,0.05)",
    borderRadius: 3,
    paddingHorizontal: 4
  },
  getStartedText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    lineHeight: 24,
    textAlign: "center"
  },
  tabBarInfoContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3
      },
      android: {
        elevation: 20
      }
    }),
    alignItems: "center",
    backgroundColor: "#fbfbfb",
    paddingVertical: 20
  },
  tabBarInfoText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    textAlign: "center"
  },
  navigationFilename: {
    marginTop: 5
  },
  helpContainer: {
    marginTop: 15,
    alignItems: "center"
  },
  helpLink: {
    paddingVertical: 15
  },
  helpLinkText: {
    fontSize: 14,
    color: "#2e78b7"
  }
});
