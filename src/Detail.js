import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { ListItem } from "react-native-elements";
import {
  Card,
  CardTitle,
  CardContent,
  CardAction,
  CardButton,
  CardImage
} from "react-native-material-cards";
import { db } from "../src/Firebase/Config";
import { Icon } from "react-native-elements";
import { SwipeListView } from 'react-native-swipe-list-view';

export default class Detail extends React.Component {
  static navigationOptions = {
    title: "Detalhes"
  };

  constructor(props) {
    super(props);
    User = db.auth().currentUser
    this.state = {
      isLoading: true,
      name: User.displayName,
      email: User.email,
      photoUrl: User.photoURL,
      emailVerified: User.emailVerified,
      uid: User.uid
    };
  }

  componentDidMount() {
    this.GetJson();
  }

  GetJson = () => {
    
    fetch(global.URL +'Service/WS/Participantes?token=' + this.state.uid + '&id=' + this.props.navigation.state.params.id)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson
          },
          function () { }
        );
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <Card style={styles.card}>
            <CardImage
              source={{
                uri: global.URL + "Image?name=" + this.props.navigation.state.params.image_scr
              }}
              title={this.props.navigation.state.params.date}
            />
            <CardTitle
              title={this.props.navigation.state.params.name}
              subtitle={this.props.navigation.state.params.subtitle}
            />
            <CardContent text={this.props.navigation.state.params.resume} />
            <CardAction>
              {this.props.navigation.state.params.bottom}
            </CardAction>
            <CardAction separator={true} />
          </Card>
          <Text style={styles.TextCenter}>Participantes Confirmados:</Text>
          <SwipeListView
            data={this.state.dataSource}
            renderItem={({ item }) => (
              <ListItem
                key={item.key}
                leftAvatar={{ source: { uri: item.avatar_url } }}
                title={item.name}
                subtitle={item.squad}
              />
            )}
            renderHiddenItem={(data, rowMap) => (
              <View style={styles.rowBack}>
                <View style={{ width: 50, backgroundColor: 'powderblue' }} />
                <Icon
                  size = '50'
                  name='heartbeat'
                  type='font-awesome'
                  color='#f50'
                  onPress={() => console.log('hello')} />
              </View>
            )}
            leftOpenValue={0}
            rightOpenValue={-150}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  TextCenter: {
    fontSize: 19,
    color: "rgba(0,0,0, 1)",
    lineHeight: 50,
    textAlign: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  card: {
    shadowColor: "#000",
    shadowOpacity: 0.1,
    shadowRadius: 1.5
  },
  rowBack: {
    backgroundColor: '#DDD',
    flex: 1,
    flexDirection: 'row-reverse',
  },
  developmentModeText: {
    marginBottom: 20,
    color: "rgba(0,0,0,0.4)",
    fontSize: 14,
    lineHeight: 19,
    textAlign: "center"
  },
  contentContainer: {
    paddingTop: 30
  },
  welcomeContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: "contain",
    marginTop: 3,
    marginLeft: -10
  },
  getStartedContainer: {
    alignItems: "center",
    marginHorizontal: 50
  },
  homeScreenFilename: {
    marginVertical: 7
  },
  codeHighlightText: {
    color: "rgba(96,100,109, 0.8)"
  },
  codeHighlightContainer: {
    backgroundColor: "rgba(0,0,0,0.05)",
    borderRadius: 3,
    paddingHorizontal: 4
  },
  getStartedText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    lineHeight: 24,
    textAlign: "center"
  },
  tabBarInfoContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: {
          width: 0,
          height: -3
        },
        shadowOpacity: 0.1,
        shadowRadius: 3
      },
      android: {
        elevation: 20
      }
    }),
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0)",
    paddingVertical: 20
  },
  tabBarInfoText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    textAlign: "center"
  },
  navigationFilename: {
    marginTop: 5
  },
  helpContainer: {
    marginTop: 15,
    alignItems: "center"
  },
  helpLink: {
    paddingVertical: 15
  },
  helpLinkText: {
    fontSize: 14,
    color: "#2e78b7"
  }
});
