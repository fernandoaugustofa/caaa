import React from 'react';
import {
  createAppContainer, createStackNavigator, createSwitchNavigator
} from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import SignInScreen from '../screens/Login';
import SignUpScreen from '../screens/SignUp';

const Login = createStackNavigator({
  Login: SignInScreen
});

const SignUp = createStackNavigator({
  SignUp: SignUpScreen
});

export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Login: Login,
    SignUp: SignUp,
    Main: MainTabNavigator,

  },
  {
    initialRouteName: 'Login',
  }
));
